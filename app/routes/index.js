const fs = require('fs');
const path = require('path');

// Require all routes inside current folder.
const requireFiles = (directory, app) => {
  fs.readdirSync(directory).forEach(async (file) => {
    if (file !== 'index.js') {
      app.use('/api/v1/', require(`${directory}/${file}`)());
    }
  });
};

module.exports = (app) => {
  requireFiles(path.join(__dirname), app);
};
