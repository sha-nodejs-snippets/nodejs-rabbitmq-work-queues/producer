const express = require('express');
const producer = require('../services/producer');

const queue = 'worker';

module.exports = () => {
  const router = express.Router();

  router.post('/tasks', async (req, res) => {
    try {
      await producer.sendToQueue(queue, req.body, { persistent: true });

      return res.status(200).json({
        message: 'Task queued.',
        statusCode: 200,
      });
    } catch (err) {
      return res.status(500).json({
        error: 'Internal server error.',
        statusCode: 500,
      });
    }
  });

  return router;
};
