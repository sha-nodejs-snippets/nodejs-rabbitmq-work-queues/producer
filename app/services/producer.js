const amqplib = require('amqplib-as-promised');
const config = require('../config');

const connectionString = `amqp://${config.RABBITMQ_USER}:${config.RABBITMQ_PASSWORD}@${config.RABBITMQ_HOST}`;

const connection = new amqplib.Connection(connectionString);
let channel;

const connect = async () => {
  await connection.init();
  channel = await connection.createChannel();
};

const sendToQueue = async (queue, payload) => {
  await channel.assertQueue(queue, {
    // Keep the queue even if empty.
    durable: true,
  });

  const buffer = Buffer.from(JSON.stringify(payload));
  await channel.sendToQueue(queue, buffer);
};

const disconnect = async () => {
  await channel.close();
  await connection.close();
};

module.exports = {
  connect,
  sendToQueue,
  disconnect,
};
