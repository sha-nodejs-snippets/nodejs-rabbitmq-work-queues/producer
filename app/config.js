// Local
const PORT = parseInt(process.env.PORT, 10);

// RabbitMQ
const RABBITMQ_USER = process.env.RABBITMQ_USER.toString();
const RABBITMQ_PASSWORD = process.env.RABBITMQ_PASSWORD.toString();
const RABBITMQ_HOST = process.env.RABBITMQ_HOST.toString();

module.exports = {
  PORT,
  RABBITMQ_USER,
  RABBITMQ_PASSWORD,
  RABBITMQ_HOST,
};
