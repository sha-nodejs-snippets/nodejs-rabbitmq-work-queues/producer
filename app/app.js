const app = require('express')();
const bodyParser = require('body-parser');
const cors = require('cors');

//  Parse request body.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Enable cors.
app.use(cors());
app.options('*', cors());

// Require all routes.
require('./routes')(app);

module.exports = app;
