require('dotenv').config({
  path: './env/.env.development',
});

const http = require('http');

const app = require('./app');
const config = require('./config');
const producer = require('./services/producer');

const server = http.createServer(app);
const port = config.PORT;

server.listen(port, async () => {
  await producer.connect();
  console.log(`Server started on port ${port}`);
});
